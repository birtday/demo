var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Blog' })
});
router.get('/post', function(req, res) {
  res.render('post')
});
router.get('/about', function(req, res) {
  res.render('about')
});
router.get('/Contact', function(req, res) {
  res.render('login')
});


module.exports = router;
